import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import axios from 'axios';
import './App.css';

// Pages
import Header from './components/header';
import Footer from './components/footer';
import Home from './components/home';
import PageNotFound from './components/pageNotFound';

const defaultState = {
    address: 'London, GBP',
    items: [],
    zoom : 9,
    center : [51.5073509,-0.12775829999998223]
};

class App extends Component {
    constructor(props) {
        super(props);
        this.state = defaultState;
        this.setGlobalState = this.setGlobalState.bind(this);
        this.getGlobalState = this.getGlobalState.bind(this);
        this.resetGlobalState = this.resetGlobalState.bind(this);
        this.getStoreList = this.getStoreList.bind(this);
    }

    setGlobalState(variableName, content) {
        let state = {...this.state};
        state[variableName] = content;
        this.setState(state);
    }
    getGlobalState(variableName) {
        let state = {...this.state};
        return (state[variableName])? state[variableName]: null; 
    }

    resetGlobalState() {
        this.setState(defaultState);
    }

    getStoreList(centerPoint) {
        if (!centerPoint) return;
        let baseUrl = "https://nameless-castle-51857.herokuapp.com/api/v1/stashpoints";
        let headers = { "Authorization": "Basic am9obi53b29AaG90bWFpbC5jby51azpwYXNzd29yZA==" }
        let params = {
            nearby_radius: 10,
            centre_lat: centerPoint.lat,
            centre_lon: centerPoint.lng
        }
        axios({
            method:'get',
            url: baseUrl,
           // headers:headers,
            params:params
          })
          .then(res => {
              this.setGlobalState("items", res.data || [])
          });
    }

    render() {
        this.getStoreList({ lat: defaultState.center[0], lng: defaultState.center[1] });
        return (
            <div className="app">
                <Router>
                    <div>
                        <Header globalState={this.state}/>
                        <Switch>
                            <Route exact path="/" render={(props) => (
                                <Home
                                    globalState={this.state}
                                    setGlobalState={this.setGlobalState}
                                    resetGlobalState={this.resetGlobalState}
                                    getStoreList={this.getStoreList}
                                />
                            )} />
                            <Route setGlobalState={this.setGlobalState} component={PageNotFound}/>
                        </Switch>
                        <Footer />
                    </div>
                </Router>
            </div>
        );
    }
}
/**
 * allow me to show the details of each stachpoint
 */
//<Route exact path="/details:id" render={(props) => ( <Details setGlobalState={this.setGlobalState} message={this.setMessage} globalState={{...this.state}}/> )} />
export default App;