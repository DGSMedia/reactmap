import React, { Component} from 'react';

export default class Marker extends Component {
  render() {    
    return (
      <a href={this.props.link} >
      <div className="markerPoint"> 
          {this.props.text}
        </div>
      </a>  
    );
  }
}