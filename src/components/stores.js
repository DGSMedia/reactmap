import React, { Component} from 'react';


export default class StoreList extends Component {

  constructor(props) {
    super(props);
  }

  render() {     
    return (
        <div className="storeList">
            {this.props.globalState.items.map((item,index) =>
          <Store key={index} item={item} index={index} />
            )}
       </div>
    );
  }
}

class Store extends Component {
  
    render() {
      return (
         <div className="storeCard" id={this.props.index}>
            <img src={this.props.item.photos[0]}  alt="asd" />
            <h3  className="title">{this.props.item.location_name}</h3>
            <div className="address">{this.props.item.address}, {this.props.item.postal_code}</div>
            <div className="capacity">{this.props.item.capacity}</div>
            <div className="actions-box">
                <button> Details </button>
                <button> Book </button>
            </div>
         </div>
      );
    }
  }