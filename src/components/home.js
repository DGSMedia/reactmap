import React, { Component } from 'react';
import SearchBox from './searchBox';
import StoreList from './stores';
import SimpleMapPage from './map';
let items = [
    {
      "address": "51 Pinfold Street",
      "bags_last_30_days": 0,
      "capacity": 18,
      "contact": {
          "email": "amanda@birmingham-mbe-luggage.co",
          "name": "Hanover Hotel",
          "phone_number": null
      },
      "country": "GBR",
      "created": "2017-11-06 12:07:28.165792",
      "description": null,
      "featured": false,
      "id": "e222b49decf5",
      "latitude": 53.992046,
      "location_name": "Oxford Bus Station",
      "longitude": -1.538261,
      "name": "MBE Birmingham",
      "open_late": true,
      "open_twentyfour_seven": false,
      "opening_hours": [
          {
              "close": "00:00:00",
              "day": 0,
              "open": "00:00:00"
          },
          {
              "close": "00:00:00",
              "day": 1,
              "open": "00:00:00"
          },
          {
              "close": "00:00:00",
              "day": 2,
              "open": "00:00:00"
          },
          {
              "close": "00:00:00",
              "day": 3,
              "open": "00:00:00"
          },
          {
              "close": "00:00:00",
              "day": 4,
              "open": "00:00:00"
          },
          {
              "close": "00:00:00",
              "day": 5,
              "open": "00:00:00"
          },
          {
              "close": "00:00:00",
              "day": 6,
              "open": "00:00:00"
          }
      ],
      "opening_hours_exceptions": [],
      "photos": [
          "https://demo.citystasher.com/albums/images/listings/1496228035_1_0.jpg",
          "https://demo.citystasher.com/albums/images/listings/117_citystasher_cambridge_heath_and_bethnal_green_stashpoint_left_luggage_at_sazzy_and_fran_cafe_0.jpg",
          "https://demo.citystasher.com/albums/images/listings/5_citystasher_paddington_stashpoint_left_luggage_at_charlies_barber_shop_0.jpg",
          "https://demo.citystasher.com/albums/images/listings/1488921627_1_2.jpg"
      ],
      "postal_code": "B2 4AY",
      "pricing_structure": {
          "ccy": "GBP",
          "extra_day_commission": 235,
          "extra_day_cost": 500,
          "first_day_commission": 285,
          "first_day_cost": 600,
          "premium_insurance": 400
      },
      "status": "active",
      "storage_forbidden_out_of_hours": false,
      "type": "other"
  }
]
items.push(items[0]);

export default class Header extends Component {
    
    constructor(props) {
        super(props);
        this.props.setGlobalState("title", "home")
        this.props.setGlobalState("items",items)
    }

    render() {
        
      return (
        <div className="content">
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 side-">
                    <SearchBox globalState={this.props.globalState} setGlobalState={ this.props.setGlobalState} getStoreList={ this.props.getStoreList}/>
                    <StoreList globalState={this.props.globalState} items={items}/>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 map" >
                    <SimpleMapPage globalState={this.props.globalState} setGlobalState={ this.props.setGlobalState}/>
                </div>
            </div>  
        </div>   
      );
    }
  }