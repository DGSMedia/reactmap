import React from 'react';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

class SearchBox extends React.Component {
  constructor(props) {
    super(props)
    this.props.setGlobalState("address", 'London, GBP');
    this.onChange = (address) => this.props.setGlobalState("address", address);
  }

  handleFormSubmit = (event) => {
    event.preventDefault()
    geocodeByAddress(this.props.globalState.address)
      .then(results => getLatLng(results[0]))
      .then(this.setCenter)
      .catch(error => console.error('Error', error))
  }

  setCenter = (latlng) => {
    this.props.setGlobalState("center", latlng)
    this.props.getStoreList(latlng);
  }

  render() {
    const inputProps = {
      value: this.props.globalState.address || 'London, GBP',
      onChange: this.onChange,
    }
    return (
      <form className="searchBox" onSubmit={this.handleFormSubmit}>
        <PlacesAutocomplete inputProps={inputProps} />
        <button type="submit">Submit</button>
      </form>
    )
  }
}

export default SearchBox