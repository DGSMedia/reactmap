import React, { Component } from 'react';

export default class Header extends Component {
  
  /*constructor(props) {
    super(props);
  }*/
  
  render() {
      return (
        <div className="App-header">
          <h2>{this.props.globalState.title}</h2>
        </div>    
      );
    }
  }