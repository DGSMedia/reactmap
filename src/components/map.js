import React, {Component} from 'react';
import GoogleMap from 'google-map-react';
import Marker from './marker';

export default class SimpleMapPage extends Component {

  render() {
    return (
       <GoogleMap
        bootstrapURLKeys='AIzaSyDt_bjc2bOyo0QYQyrXsRhNQ6dRrYwp6m4' // set if you need stats etc ...
        center={this.props.globalState.center}
        zoom={this.props.globalState.zoom}>
        {this.props.globalState.items.map((item,index) =>
          <Marker key={index} lat={item.latitude} lng={item.longitude} text={index} link={'#'+index}/>
        )}
      </GoogleMap>
    );
  }
}